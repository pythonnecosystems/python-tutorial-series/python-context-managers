# Python 컨텍스트 관리자: with 문과 커스텀

## <a name="intro"></a> 개요
이 포스팅에서는 Python에서 컨텍스트 매니저를 사용하는 방법에 대해 설명할 것이다. 컨텍스트 매니저는 획득하고 릴리스해야 하는 리소스를 제어된 방식으로 관리하는 방법이다. 예를 들어 파일을 열었다면 파일을 다 사용한 후에는 파일을 닫아야 한다. 컨텍스트 매니저는 파일을 자동으로 닫음으로써 이 프로세스를 더 쉽고 안정적으로 만든다.

컨텍스트 관리자는 종종 코드 블록에 대한 컨텍스트를 생성하는 `with` 문과 함께 사용된다. `with` 문은 컨텍스트 관리자의 **`enter`** 와 **`exit`** 메서드가 코드 블록 이전과 이후에 각각 호출되는 것을 보장한다. `enter` 메서드는 코드 블록 내부에서 사용할 수 있는 개체를 반환할 수 있고, `exit` 메서드는 필요한 모든 정리 작업을 수행할 수 있다.

Python은 파일 열기, 스레드 잠금, 예외 처리 등과 같은 일반적인 작업을 위한 내장된 컨텍스트 매니저를 제공한다. 클래스나 생성기를 사용하여 자신만의 맞춤형 컨텍스트 매니저를 만들 수도 있다. 이 포스팅에서는 두 가지 모두 수행하는 방법을 설명할 것이다.

이 글을 읽고 이해한다면 다음 작업을 수행할 수 있다.

- 내장된 컨텍스트 관리자에 대해 문과 함께 사용
- 클래스를 사용하여 커스텀 컨텍스트 관리자 만들기
- 생성기를 사용하여 커스텀 컨텍스트 관리자 만들기
- 일반적인 컨텍스트 관리자 패턴에 `contextlib` 모듈 사용

시작합시다!

## <a name="sec_02"></a> 컨텍스트 관리자는 무엇이며 왜 유용한가?
컨텍스트 관리자(context manager)는 코드 블록에 대한 런타임 컨텍스트를 정의하는 객체이다. 이는 `__enter__`와 `__exit__` 두 메서드를 가진다. `__enter__` 메서드는 컨텍스트에 들어갈 때 호출되며 컨텍스트 내부에서 사용할 수 있는 객체를 반환할 수 있다. `__exit__` 메서드는 컨텍스트가 종료될 때 호출되며 필요한 모든 정리 작업을 수행한다.

컨텍스트 매니저는 획득하고 릴리스해야 하는 리소스의 관리를 통제된 방식으로 단순화하기 때문에 유용하다. 예를 들어, 파일을 열고 파일을 사용한 후에는 닫아야 한다. 파일 닫기를 하지 않은 경우 리소스 유출이나 파일 손상이 발생할 수 있다. 컨텍스트 매니저는 파일을 자동으로 닫음으로써 이 프로세스를 더 쉽고 신뢰할 수 있게 해준다.

다음은 파일을 열고 닫기 위해 컨텍스트 관리자를 사용하는 방법의 예이다.

```python
# Without a context manager
f = open("file.txt", "w") # Open the file
f.write("Hello, World!") # Write some data
f.close() # Close the file
# With a context manager
with open("file.txt", "w") as f: # Open the file and assign it to f
    f.write("Hello, World!") # Write some data
# The file is automatically closed when the with block ends
```

보다시피 컨텍스트 관리자는 코드를 더 짧고 깨끗하게 만든다. 또한 with 블록 내부에서 예외가 발생하더라도 파일이 닫히게 한다.

컨텍스트 관리자는 파일에 국한되지 않는다. 네트워크 연결, 데이터베이스 트랜잭션, 잠금, 타이머 등과 같은 다른 유형의 리소스에도 사용할 수 있다. Python은 파일 열기, 스레드 잠금, 예외 처리 등과 같은 일반적인 작업을 위한 일부 내장 컨텍스트 관리자를 제공한다. 클래스나 생성기를 사용하여 자신만의 커스텀 컨텍스트 관리자를 만들 수도 있다. 다음 절에서 두 가지 모두를 수행하는 방법을 배울 것이다.

## <a name="sec_03"></a> 내장된 컨텍스트 관리자에 대해 문과 함께 사용하는 방법
**`with`** 문은 코드 블록에 대한 컨텍스트를 생성하는 구문이다. 컨텍스트 관리자를 인수로 사용하고 코드 블록 이전과 이후에 각각 `__enter__`와 `__exit__` 메서드를 호출한다. `__enter__` 메서드는 코드 블록 내부에서 사용할 수 있는 객체를 반환할 수 있으며 `__exit__` 메서드는 필요한 모든 정리 작업을 수행할 수 있다.

with 문의 일반적인 구문은 다음과 같다.

```python
with context_manager as variable:
    # Do something with variable
```

`context_manager`는 `__enter__`와 `__exit__` 메서드를 구현하는 임의의 객체일 수 있다. **`variable`**는 선택적이며 `__enter__` 메서드가 반환한 객체를 액세스하는 데 사용할 수 있다. `variable`이 지정되지 않은 경우 `__enter__` 메서드에 의해 반환된 객체는 폐기된다.

Python은 파일 열기, 스레드 잠금, 예외 처리와 같은 일반적인 작업을 위한 내장 컨텍스트 관리자를 제공한다. 다음은 문장과 함께 그들을 사용하는 예이다.

- **파일 열기 및 닫기**: `open` 함수는 컨텍스트 관리자로 사용할 수 있는 파일 객체를 반환한다. 예외가 발생하면 `with` 블록이 종료되면 자동으로 파일을 닫는다. 예를 들면 다음과 같다.

```python
# Open a file for reading
with open("file.txt", "r") as f:
    # Read the file contents
    data = f.read()
    # Do something with data
    print(data)
# The file is automatically closed
```

- **잠금과 잠금 해제 스레드**: `threading.Lock` 클래스는 컨텍스트 관리자로 사용할 수 있는 잠금 객체를 반환한다. with 블록이 시작되고 종료되면 자동으로 잠금을 획득하고 해제한다. 이를 통해 한 번에 하나의 스레드만 공유 리소스를 액세스할 수 있다. 예를 들면 다음과 같다.

```python
import threading
# Create a lock object
lock = threading.Lock()
# Define a function that uses the lock
def increment(counter):
    # Acquire the lock
    with lock:
        # Increment the counter
        counter += 1
        # Do something with counter
        print(counter)
    # Release the lock
# Create a shared counter
counter = 0
# Create two threads that call the function
t1 = threading.Thread(target=increment, args=(counter,))
t2 = threading.Thread(target=increment, args=(counter,))
# Start the threads
t1.start()
t2.start()
# Wait for the threads to finish
t1.join()
t2.join()
```

- **예외 처리**: `contextlib.suppress` 함수는 with 블럭 내부에서 발생하는 지정된 예외를 억제하는 컨텍스트 관리자를 반환한다. 이는 예상되거나 무해한 특정 오류를 무시하는 데 유용할 수 있다. 예를 들어,

```python
import contextlib
# Define a list of numbers
numbers = [1, 2, 3, 4, 5]
# Try to access an invalid index
with contextlib.suppress(IndexError):
    # This will raise an IndexError
    print(numbers[10])
# The exception is suppressed and the program continues
print("Done")
```

이들은 Python이 제공하는 내장된 컨텍스트 매니저 중 일부일 뿐이다. 이에 대한 자세한 내용은 [표준 라이브러리](https://docs.python.org/3/library/index.html)에서 찾을 수 있다. 클래스나 생성기를 사용하여 커스텀 컨텍스트 매니저를 만들 수도 있다. 다음 절에서는 이 두 가지를 모두 수행하는 방법에 대해 설명한다.

## <a name="sec_04"></a> 클래스를 사용하여 커스텀 컨텍스트 관리자를 만드는 방법
커스텀 컨텍스트 관리자를 만들고 싶다면 `__enter__`와 `__exit__` 메서드를 구현하는 클래스를 정의하면 된다. `__enter__` 메서드는 컨텍스트에 들어갈 떄 호출되며 컨텍스트 내부에서 사용할 수 있는 객체를 반환할 수 있다. `__exit__` 메서드는 컨텍스트가 종료되면 호출되며 필요한 모든 정리 작업을 수행할 수 있다.

다음은 클래스를 사용하여 커스텀 컨텍스트 관리자를 만드는 방법의 예이다.

```python
# Import the time module
import time

# Define a custom context manager class
class Timer:
    # Define the __enter__ method
    def __enter__(self):
        # Record the start time
        self.start = time.time()
        # Return the timer object
        return self
    # Define the __exit__ method
    def __exit__(self, exc_type, exc_value, exc_traceback):
        # Record the end time
        self.end = time.time()
        # Calculate the elapsed time
        self.elapsed = self.end - self.start
        # Print the elapsed time
        print(f"Elapsed time: {self.elapsed} seconds")
# Use the custom context manager with the with statement
with Timer() as t:
    # Do some computation
    result = sum(range(1000000))
    # Print the result
    print(result)
# The timer object is automatically deleted and the elapsed time is printed
```

보다시피 커스텀 컨텍스트 매니저 클래스는 자원 관리를 처리하는 `__enter__`와 `__exit__` 메서드를 정의한다. `__enter__` 메서드는 `with` 블럭 내부에서 사용할 수 있는 timer 객체를 반환한다. `__exit__` 메서드는 `start` 시간과 `end` 시간으로부터 계산된 `elapsed` 시간을 출력한다.

클래스를 사용하여 커스텀 컨텍스트 관리자를 생성하는 것은 내장된 컨텍스트 관리자가 지원하지 않는 리소스를 관리하는 데 유용할 수 있다. 그러나 특히 단순한 컨텍스트 관리자만 필요한 경우에는 장황하고 번거로울 수도 있다. 그런 경우 생성기를 사용하여 커스텀 컨텍스트 관리자를 보다 쉽게 생성할 수 있다. 다음 절에서 그 방법에 대해 설명할 것이다.

## <a name="sec_05"></a> 생성기를 사용하여 커스텀 컨텍스트 관리자를 만드는 방법
커스텀 컨텍스트 매니저를 만들고 싶다면 생성기 함수를 사용하여 생성할 수도 있다. 생성기 함수는 `yield` 키워드를 사용하여 반복할 수 있는 반복기를 반환하는 함수이다. `contextlib` 모듈에서 `@contextmanager` 데코레이터를 사용하여 생성기 함수를 컨텍스트 매니저로 변환할 수 있다. `@contextmanager` 데코레이터는 `__enter__` 메서드와 `__exit__` 메서드를 호출하는 역할을 대신 수행한다.

생성기 함수를 사용하여 커스텀 컨텍스트 관리자를 만드는 일반 구문은 다음과 같다.

```python
# Import the contextlib module
import contextlib
# Define a generator function with the @contextmanager decorator
@contextlib.contextmanager
def context_manager():
    # Do something before yielding
    # This is equivalent to the __enter__ method
    # You can optionally return an object that can be used inside the context
    yield object
    # Do something after yielding
    # This is equivalent to the __exit__ method
    # You can optionally handle any exceptions that occur inside the context
```

다음은 생성기 함수를 사용하여 커스텀 컨텍스트 관리자를 만드는 방법의 예이다.

```python
# Import the contextlib module
import contextlib
# Define a generator function with the @contextmanager decorator
@contextlib.contextmanager
def reversed_list(lst):
    # Reverse the list before yielding
    lst.reverse()
    # Yield the reversed list
    yield lst
    # Reverse the list again after yielding
    lst.reverse()
# Use the custom context manager with the with statement
with reversed_list([1, 2, 3, 4, 5]) as lst:
    # Do something with the reversed list
    print(lst)
# The list is restored to its original order
print(lst)
```

위의 코드에서 커스텀 컨텍스트 매니저 함수는 리스트를 `yield`하기 전과 후에 반전시킨다. 반전된 목록은 `with` 블록 내부에서 사용할 수 있다. `with` 블록이 종료되면 원래의 리스트는 복원된다.

생성기를 사용하여 커스텀 컨텍스트 매니저를 만드는 것은 코드를 단순화하고 `__enter__`와 `__exit__` 메서드로 클래스를 정의하는 보일러 플레이트를 피하는 데 유용할 수 있다. 그러나 생성기 함수에서 한 번만 양보할 수 있다는 사실에 의해 제한될 수도 있다. 유연성이 더 필요한 경우 일반적인 컨텍스트 매니저 패턴에 `contextlib` 모듈을 사용할 수 있다. 다음 섹션에서 그 방법을 설명한다.

## <a name="sec_06"></a> 일반적인 컨텍스트 관리자 패턴에 contextlib 모듈을 사용하는 방법
**`contextlib`** 모듈은 컨텍스트 관리자를 생성하고 작업하는 데 유용한 유틸리티를 제공하는 표준 라이브러리 모듈이다. 코드를 단순화하고 동일한 로직을 반복하지 않도록 하는 데 사용할 수 있는 몇 가지 일반적인 컨텍스트 관리자 패턴도 제공한다. 다음은 일반적인 컨텍스트 관리자 패턴에 `contextlib` 모듈을 사용하는 방법의 예이다.

- **`close()` 메서드를 지원하는 객체 닫기**: `contextlib.closing` 함수는 컨텍스트가 종료될 때 객체의 `close()` 메서드를 호출하는 컨텍스트 관리자를 반환한다. 이는 컨텍스트 관리자 프로토콜을 지원하지 않지만 호출해야 하는 `close()` 메서드를 가진 객체에게는 유용할 수 있다. 예를 들면 다음과 같다.

```python
# Import the contextlib module
import contextlib
# Import the urllib.request module
import urllib.request
# Open a URL using the urllib.request.urlopen function
# This function returns an object that supports the close() method, but not the context manager protocol
url = urllib.request.urlopen("https://www.bing.com")
# Use the contextlib.closing function to create a context manager for the URL object
with contextlib.closing(url) as u:
    # Do something with the URL object
    data = u.read()
    # Print the data
    print(data)
# The URL object is automatically closed when the with block ends
```

- **표준 출력 또는 오류 스트림 리디렉션**: `contextlib.redirect_stdout`과 `contextlib.redirect_stderr` 함수는 표준 출력 또는 오류 스트림을 주어진 파일과 같은 객체로 리디렉션하는 컨텍스트 관리자를 반환한다. 이는 코드 블록이 인쇄하는 출력 또는 오류 메시지를 캡처하거나 억제하는 데 유용할 수 있다. 예를 들어,

```python

# Import the contextlib module
import contextlib
# Import the sys module
import sys
# Define a function that prints some messages to the standard output and error streams
def print_messages():
    print("This is a normal message")
    print("This is an error message", file=sys.stderr)
# Use the contextlib.redirect_stdout function to redirect the standard output to a file
with open("output.txt", "w") as f:
    with contextlib.redirect_stdout(f):
        # Call the function
        print_messages()
# The standard output is restored and the messages are written to the file
# Use the contextlib.redirect_stderr function to redirect the standard error to another file
with open("error.txt", "w") as f:
    with contextlib.redirect_stderr(f):
        # Call the function again
        print_messages()
# The standard error is restored and the messages are written to another file
```

- **현재 작업 디렉터리 임시로 변경**: `contextlib.chdir` 함수는 컨텍스트가 입력되면 현재 작업 디렉터리를 주어진 경로로 변경하고 컨텍스트가 종료되면 복원하는 컨텍스트 관리자를 반환한다. 이는 현재 작업 디렉터리에 의존하는 코드 블록을 실행하는 데 유용할 수 있다. 예를 들어:

```python
# Import the contextlib module
import contextlib
# Import the os module
import os
# Print the current working directory
print(os.getcwd())
# Use the contextlib.chdir function to change the current working directory to a different path
with contextlib.chdir("/tmp"):
    # Print the current working directory
    print(os.getcwd())
    # Do something in the new working directory
    os.mkdir("test")
# The current working directory is restored and the test directory is created in /tmp
```

이들은 `contextlib` 모듈이 제공하는 일반적인 컨텍스트 매니저 패턴 중 일부에 불과하다. 자세한 내용은 [문서]()에서 확인할 수 있다. 앞 절에서 배운 것처럼 생성기를 사용하여 자신만의 커스텀 컨텍스트 매니저를 만들기 위하여 수도 `contextlib` 모듈을 사용할 수 있다.

## <a name="summary"></a> 요약
이 포스팅에서는 Python에서 컨텍스트 매니저를 사용하는 방법을 배웠다. 컨텍스트 매니저는 획득하고 놓아야 하는 리소스를 통제된 방식으로 관리하는 방법이다. 내장된 컨텍스트 매니저에 대해 with 문을 사용하는 방법, 클래스나 생성기를 사용하여 커스텀 컨텍스트 매니저를 만드는 방법, 일반적인 컨텍스트 매니저 패턴에 대해 `contextlib` 모듈을 사용하는 방법을 설명하였다. 컨텍스트 매니저를 사용하면 코드를 단순화하여 보다 신뢰성 있고 견고하게 만들 수 있다.
